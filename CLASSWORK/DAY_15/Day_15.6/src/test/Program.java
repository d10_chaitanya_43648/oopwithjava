package test;

import java.util.Arrays;

public class Program {
	private static void print(int[] arr) {
		if( arr != null )
			System.out.println(Arrays.toString(arr));
		System.out.println();
	}
	public static void main(String[] args) {
		int[] arr = new int[] { 5, 1, 4, 2, 3 };
		Program.print( arr );	//[5, 1, 4, 2, 3]
		Arrays.sort(arr);	//Dual-Pivot Quicksort
		Program.print( arr );	//[1, 2, 3, 4, 5]
	}
}