package test;

interface A{
	void f1( );
	void f3( );
}
interface B{
	void f2( );
	void f3( );
}
abstract class C{
	public abstract void f3( );
}
class D extends C implements A, B{
	@Override
	public void f1() {
		System.out.println("D.f1");
	}
	@Override
	public void f2() {
		System.out.println("D.f2");
	}
	@Override
	public void f3() {
		System.out.println("D.f3");
	}
}
public class Program {
	public static void main(String[] args) {
		A a = new D( );
		a.f1();
		a.f3();
		
		System.out.println();
		
		B b = new D( );
		b.f2();
		b.f3();
		
		System.out.println();
		
		C c = new D( );
		c.f3();
	}
}
