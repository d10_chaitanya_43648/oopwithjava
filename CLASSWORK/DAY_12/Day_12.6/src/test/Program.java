package test;
class A{
	public void f1( ) {
		System.out.println("A.f1");
	}
	public void f2( ) {
		System.out.println("A.f2");
	}
}
class B extends A{
	public void f1( ) {
		System.out.println("B.f1");
	}
	public void f3( ) {
		System.out.println("B.f3");
	}
}
public class Program {
	public static void main(String[] args) {
		A a = new B( );	//Upcasting
		//a.f1( );	//Late Binding : B.f1
		//a.f2();	//Late Binding : A.f2
		//a.f3( );	//Error
		
		B b  = (B) a;
		b.f1();
	}
}
