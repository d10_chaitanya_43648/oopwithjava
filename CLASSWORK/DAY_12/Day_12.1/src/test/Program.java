package test;

import java.util.Scanner;

class Book{
	private String title;
	private float price;
	private int pageCount;
	public Book() {
	}
	public Book(String title, float price, int pageCount) {
		this.title = title;
		this.price = price;
		this.pageCount = pageCount;
	}
	public void acceptRecord() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Title		:	");
		this.title = sc.nextLine();
		System.out.print("Price		:	");
		this.price = sc.nextFloat();
		System.out.print("Page Count	:	");
		this.pageCount = sc.nextInt();
	}
	public void printRecord() {
		System.out.println("Title		:	"+this.title);
		System.out.println("Price		:	"+this.price);
		System.out.println("Page Count	:	"+this.pageCount);
	}
}
public class Program {
	public static void main(String[] args) {
		Book book = new Book( );
		book.acceptRecord( );
		book.printRecord( );
	}
	public static void main1(String[] args) {
		Book book = new Book("Let Us C", 350.50f, 560 );
		book.printRecord( );
	}
}
