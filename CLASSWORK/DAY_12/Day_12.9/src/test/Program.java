package test;
class A{
	public void f1( int num1 ) {
		System.out.println("A.f1");
	}
}
class B extends A{
	@Override
	public void f1( int num1 ) {
		System.out.println("B.f1");
	}
}
public class Program {
	public static void main(String[] args) {
		A a = new B( );
		a.f1(10);
	}
}