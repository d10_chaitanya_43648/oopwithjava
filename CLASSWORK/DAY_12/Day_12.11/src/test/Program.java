package test;
class Rectangle{
	private double area;
	private double length;
	private double breadth;
	public Rectangle() {
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	public void setBreadth(double breadth) {
		this.breadth = breadth;
	}
	public void calculateArea( ) {
		this.area = this.length * this.breadth;
	}
	public double getArea() {
		return this.area;
	}
}
class Circle{
	private double area;
	private double radius;
	public Circle() {
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public void calculateArea( ) {
		this.area = Math.PI * Math.pow(this.radius, 2);
	}
	public double getArea() {
		return this.area;
	}
}
public class Program {
	public static void main(String[] args) {
		Circle c = new Circle();
		c.setRadius(10);
		c.calculateArea();
		System.out.println("Area	:	"+c.getArea());
	}
	public static void main1(String[] args) {
		Rectangle rect = new Rectangle();
		rect.setLength(10);
		rect.setBreadth(20);
		rect.calculateArea();
		System.out.println("Area	:	"+rect.getArea());
	}
}