package test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
public class Program {
	private static void printFileInfo(File file) throws Exception{
		System.out.println("Name	:	"+file.getName());
		System.out.println("Parent	:	"+file.getParent());
		System.out.println("Length	:	"+file.length());
		System.out.println("Last Modified	:	"+new SimpleDateFormat("dd-MMMM,yyyy").format(new Date(file.lastModified())));
	}

	private static void printDirectoryInfo(File file) {
		File[] files = file.listFiles();
		for (File f : files) {
			if( !f.isHidden())
				System.out.println(f.getName());
		}
	}
	public static void main(String[] args) {
		try( Scanner sc = new Scanner(System.in)){
			System.out.print("F.Q.Name	:	");
			String pathname = sc.nextLine();
			File file = new File(pathname);
			if( file.exists()) {
				if( file.isDirectory())
					Program.printDirectoryInfo(file );
				else
					Program.printFileInfo(file );
			}else
				System.out.println(pathname+" does not exist");
		}catch( Exception ex ) {
			ex.printStackTrace();
		}
	}
}
