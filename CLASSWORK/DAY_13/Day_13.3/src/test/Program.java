package test;
class Employee{
	private String name;
	private int empid;
	private String department;
	private String designation;
	private float salary;
	public Employee() {
	}
	public Employee(String name, int empid, String department, String designation, float salary) {
		this.name = name;
		this.empid = empid;
		this.department = department;
		this.designation = designation;
		this.salary = salary;
	}
	public void printRecord( ) {
		System.out.println("Name		:	"+this.name);
		System.out.println("Empid		:	"+this.empid);
		System.out.println("Department	:	"+this.department);
		System.out.println("Designation	:	"+this.designation);
		System.out.println("Salary		:	"+this.salary);
	}
	@Override
	public String toString() {
		//return this.name+" "+this.empid+" "+this.department+" "+this.designation+" "+this.salary;
		//return this.name+" "+this.empid+" "+this.salary;
		return String.format("%-20s%-4d%-10.2f", this.name, this.empid, this.salary);
	}
	/*@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + "]";
	}*/
}
public class Program {
	public static Employee[] getEmployees( ) {
		Employee[] arr = new Employee[ 3 ];
		arr[ 0 ]  = new Employee("Prashant Papal", 5, "Account", "Manager", 35000);
		arr[ 1 ] = new Employee("Sanjay Choudhari", 3, "SAN", "Ass. Director", 30000);
		arr[ 2 ] = new Employee("Nitin Kudhale", 2,  "TCT", "Director", 45000);
		return arr;
	}
	private static void printRecord(Employee[] employees) {
		if( employees != null ) {
			for (Employee employee : employees) {
				System.out.println(employee.toString());
			}
		}
	}
	public static void main(String[] args) {
		Employee[] employees = Program.getEmployees();
		Program.printRecord( employees );
	}
	
}
