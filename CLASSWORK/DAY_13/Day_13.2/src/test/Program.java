package test;
class Employee{
	private String name;
	private int empid;
	private String department;
	private String designation;
	private float salary;
	public Employee() {
	}
	public Employee(String name, int empid, String department, String designation, float salary) {
		this.name = name;
		this.empid = empid;
		this.department = department;
		this.designation = designation;
		this.salary = salary;
	}
	public void printRecord( ) {
		System.out.println("Name		:	"+this.name);
		System.out.println("Empid		:	"+this.empid);
		System.out.println("Department	:	"+this.department);
		System.out.println("Designation	:	"+this.designation);
		System.out.println("Salary		:	"+this.salary);
	}
	/*@Override
	public String toString() {
		//return this.name+" "+this.empid+" "+this.department+" "+this.designation+" "+this.salary;
		//return this.name+" "+this.empid+" "+this.salary;
		return String.format("%-20s%-4d%-10.2f", this.name, this.empid, this.salary);
	}*/
	@Override
	public String toString() {
		return "Employee [name=" + name + ", empid=" + empid + ", salary=" + salary + "]";
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee("Sandeep Kulange", 33, "TCT", "Technical Head", 45000.50f);
		System.out.println(emp);
	}
	public static void main2(String[] args) {
		Employee emp = new Employee("Sandeep Kulange", 33, "TCT", "Technical Head", 45000.50f);
		System.out.println(emp.toString());
	}
	public static void main1(String[] args) {
		Employee emp = new Employee("Sandeep Kulange", 33, "TCT", "Technical Head", 45000.50f);
		String str = emp.toString();
		System.out.println(str);
	}
}
