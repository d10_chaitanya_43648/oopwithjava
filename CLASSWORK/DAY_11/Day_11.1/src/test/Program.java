package test;
class Person{
	String name;
	int age;
	public Person( ) {
		this.name = "Sandeep";
		this.age = 37;
	}
	public Person( String name, int age ) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
	public void printRecord( ) {
		System.out.println("Person.printRecord");
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
class Employee extends Person{
	
}
public class Program {
	public static void main(String[] args) {
		Employee emp = new Employee();
		//emp.showRecord();	//OK
		emp.printRecord();	//OK
	}
	public static void main3(String[] args) {
		Employee emp = new Employee();
		System.out.println("Name	:	"+emp.name);
		System.out.println("Age	:	"+emp.age);
	}
	public static void main2(String[] args) {
		Person p = new Person("Rahul", 40);	//Here Parameteized constructor will call.
		//p.showRecord();	//OK
		//p.printRecord();	//OK
	}
	public static void main1(String[] args) {
		Person p = new Person();	//Here Parameterless constructor will call.
		//p.showRecord();	//OK
		//p.printRecord();	//OK
	}
}
