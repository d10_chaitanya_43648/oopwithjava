package test;
class Base{
	private int num1;
	private int num2;
	public Base( ) {
		this.num1 = 10;
		this.num2 = 20;
	}
	public void show( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
}
class Derived extends Base{
	private int num3;
	public Derived() {
		this.num3 = 30;
	}
	public void print( ) {	//Method Overriding
		super.print();
		System.out.println("Num3	:	"+this.num3);
	}
	public void display( ) {
		super.show();
		System.out.println("Num3	:	"+this.num3);
	}
}
public class Program {
	public static void main(String[] args) {
		Base base = new Derived();	//Upcasting
		base.display( );	//Error
	}
	public static void main3(String[] args) {
		Base base = new Derived();	//Upcasting
		base.show();
	}
	public static void main2(String[] args) {
		Base base = new Derived();	//Upcasting
		base.print();	//Runtime Polymorphism / Dynamic Method Dispatach
	}
	public static void main1(String[] args) {
		Derived derived = new Derived();
		derived.print();
	}
}
