package test;
class Person{
	
}
class Employee extends Person{
	int empid;
	float salary;
	public Employee() {
		this.empid = 33;
		this.salary = 24000.50f;
	}
	
	public Employee(int empid, float salary) {
		this.empid = empid;
		this.salary = salary;
	}

	public void printRecord( ) {
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
	public void displayRecord( ) {
		System.out.println("Empid	:	"+this.empid);
		System.out.println("Salary	:	"+this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Person p = new Person();
		//p.printRecord();	//OK
		//p.displayRecord();	//OK
	}
	public static void main3(String[] args) {
		Person p = new Person();
		//System.out.println("Empid	:	"+p.empid);		//Not OK
		//System.out.println("Salary	:	"+p.salary);	//Not OK
	}
	public static void main2(String[] args) {
		Employee emp = new Employee(123, 50000);
		//emp.printRecord();	//OK
		//emp.displayRecord();	//OK
	}
	public static void main1(String[] args) {
		Employee emp = new Employee();
		//emp.printRecord();	//OK
		//emp.displayRecord();	//OK
	}
}
