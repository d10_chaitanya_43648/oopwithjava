package test;
class Base{
	int num1;
	int num2;
	public Base( ) {
		this.num1 = 10;
		this.num2 = 20;
	}
	public Base( int num1, int num2 ) {
		this.num1 = num1;
		this.num2 = num2;
	}
	public void showRecord( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
}
class Derived extends Base{
	int num3;
	public Derived() {
		this.num3 = 30;
	}
	public Derived( int num1, int num2, int num3 ){
		super( num1, num2 );
		this.num3 = num3;
	}
	public void displayRecord( ) {
		super.showRecord();
		System.out.println("Num3	:	"+this.num3);
	}
}
public class Program {
	public static void main(String[] args) {
		Base base = new Derived();	//Upcasting : OK
		
	}
	public static void main3(String[] args) {
		Base base = new Derived();	//Upcasting : OK
		base.showRecord();//OK
		//base.displayRecord( );	//NOT OK
	}

	public static void main2(String[] args) {
		Base base = new Derived();	//Upcasting : OK
		System.out.println("Num1	:	"+base.num1);	//OK
		System.out.println("Num2	:	"+base.num2);	//OK
		//System.out.println("Num3	:	"+base.num3);	//OK
	}
	public static void main1(String[] args) {
		Derived derived = new Derived();
		derived.displayRecord();
		//Base base = ( Base )derived;	//Upcasting : OK
		Base base = derived;	//Upcasting : OK
		base.showRecord();
	}
}
