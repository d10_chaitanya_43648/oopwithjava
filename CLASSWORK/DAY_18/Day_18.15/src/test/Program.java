package test;

import java.util.Scanner;

public class Program {	
	public static void main(String[] args) {
		try( Scanner sc = new Scanner(System.in)){
			System.out.print("Number	:	");
			System.out.println("Number	:	"+Integer.parseInt(new StringBuilder(String.valueOf(sc.nextInt())).reverse().toString()));
		}
	}
	public static void main1(String[] args) {
		try( Scanner sc = new Scanner(System.in)){
			System.out.print("Number	:	");
			int number = sc.nextInt();
			System.out.println("Number	:	"+number);
			
			String strNumber = String.valueOf(number);
			StringBuilder sb = new StringBuilder(strNumber);
			sb.reverse();
			strNumber = sb.toString();
			
			number = Integer.parseInt(strNumber);
			System.out.println("Number	:	"+number);
		}
	}
}
