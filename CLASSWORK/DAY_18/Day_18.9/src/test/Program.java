package test;

import java.util.ArrayList;
import java.util.List;

public class Program {	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<>( );
		list.add(10);
		list.add(20);
		list.add(30);
		
		//Consumer<Integer> action = ( Integer number)-> System.out.println(number);
		//list.forEach(action);
		
		list.forEach( ( Integer number)-> System.out.println(number) );
	}
}
