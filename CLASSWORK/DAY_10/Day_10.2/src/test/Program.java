package test;

class Person{
	private String name;
	private int age;
	public Person() {
		this("",0);
	}
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	public void showRecord( ) {
		System.out.println("Name	:	"+this.name);
		System.out.println("Age	:	"+this.age);
	}
}
public class Program {
	public static void main(String[] args) {
		Person p = new Person("Sandeep", 37 );
		p.showRecord();
	}
	public static void main1(String[] args) {
		Person p = new Person();
		p.showRecord();
	}
}
