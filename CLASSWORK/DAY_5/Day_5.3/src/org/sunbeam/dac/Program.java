package org.sunbeam.dac;
class Test{
	private int num1;	//Instance variable
	private int num2;	//Instance variable
	private int num3;	//Instance variable
	public Test(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
		this.num3 = 500;
	}
	public void printRecord( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
		System.out.println("Num3	:	"+this.num3);
		System.out.println();
	}
}
public class Program {	
	public static void main(String[] args) {
		Test t1 = new Test( 10, 20 );
		Test t2 = new Test( 30, 40 );
		Test t3 = new Test( 50, 60 );
		
		t1.printRecord( );
		t2.printRecord( );
		t3.printRecord( );
	}
}
