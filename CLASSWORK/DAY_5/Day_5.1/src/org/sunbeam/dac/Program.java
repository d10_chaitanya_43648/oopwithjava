package org.sunbeam.dac;

import java.util.Scanner;

//Write a class to accept and print information of employee.
/*
 * Employee information : name, empid, salary 
 * Operation : accept, print
*/
class Employee{
	private String name;	//null
	private int empid;		//0
	private float salary;	//0.0
	
	public Employee() {
		this( "",0,0);	//Constructor Chaining -> Explicit Call 
	}
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	public void acceptRecord( /*Employee this*/ ) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Name	:	");
		this.name = sc.nextLine();
		System.out.print("Empid	:	");
		this.empid = sc.nextInt();
		System.out.print("Salary	:	");
		this.salary = sc.nextFloat();
	}
	public void printRecord( /*Employee this*/ ) {
		System.out.println("Name	:	"+ this.name);
		System.out.println("Empid	:	"+ this.empid);
		System.out.println("Salary	:	"+ this.salary);
	}
}
public class Program {
	public static void main(String[] args) {
		Employee e1 = new Employee();
		e1.acceptRecord();	//e1.acceptRecord( e1 );;
		e1.printRecord();	//e1.printRecord( e1 );
		
		Employee e2 = new Employee();
		e2.acceptRecord();	//e2.acceptRecord( e2 );
		e2.printRecord();	//e2.printRecord( e2 );
		
		Employee e3 = new Employee();
		e3.acceptRecord();	//e3.acceptRecord( e3 );
		e3.printRecord( );	//e3.printRecord( e3 );
	}
	public static void main3(String[] args) {
		Employee e1 = new Employee("Akash", 11, 15000);
		Employee e2 = new Employee("Ketan", 12, 17000);
		Employee e3 = new Employee("Yoesh", 13, 16000);	
	}
	public static void main2(String[] args) {
		Employee emp = new Employee(  );
		emp.printRecord( );	//emp.printRecord( emp );	
	}
	public static void main1(String[] args) {
		Employee emp = new Employee( "Sandeep", 33, 34000.50f );
		emp.printRecord( );	//emp.printRecord( emp );	
	}
}
