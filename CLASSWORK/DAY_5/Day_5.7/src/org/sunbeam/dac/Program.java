package org.sunbeam.dac;
class Calculator{
	public double power( double base, int index ) {
		double result = 1;
		for( int count = 1; count <= index; ++ count )
			result = result * base;
		return result;
	}
}
public class Program {
	public static void main(String[] args) {
		Calculator c = new Calculator();
		double result =  c.power(5.0, 2);
		System.out.println(result);
	}
}

