package test;

import java.util.Date;

//Parameterized Class/Type
class Box<T>{	//T	=>	Type Parameter
	private T object;
	public T getObject() {
		return object;
	}
	public void setObject(T object) {
		this.object = object;
	}
}
public class Program {
	public static void main(String[] args) {
		Box<Integer> b1 = new Box<>( );	
		Box<Double> b3 = new Box<>( );	
		Box<String> b4 = new Box<>( );	
	}
	public static void main4(String[] args) {
		//Box<int> box = new Box<>( );	//int => NOT OK
		Box<Integer> box = new Box<>( );	//Integer => OK
	} 
	public static void main3(String[] args) {
		Box box = new Box( );	//Box=> Raw Type
		//Box<Object> box = new Box<>( );
	}
	public static void main2(String[] args) {
		Box<Date> box = new Box< >( );	
		
		box.setObject(new Date());
		Date date = box.getObject();
		System.out.println(date);
	}
	public static void main1(String[] args) {
		Box<Date> box = new Box<Date>( );	//Date	=> Type Argument
		box.setObject(new Date());
		Date date = box.getObject();
		System.out.println(date);
	}
}
