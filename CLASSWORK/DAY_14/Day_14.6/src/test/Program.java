package test;

import java.util.Date;

class Box{
	private Object object;
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
}
public class Program {
	public static void main(String[] args) {
		Box box = new Box();	//OK
		box.setObject( new Date( ) );	//OK
		
		String str =  (String) box.getObject();	//Downcasting :  ClassCastException 
		System.out.println(str);
		
		//Date date = (Date) box.getObject();
		//System.out.println(date);
	}
	public static void main3(String[] args) {
		Box box = new Box();	//OK
		box.setObject( new Date( ) );	//OK	
	}
	public static void main2(String[] args) {
		Box box = new Box();	//OK
		box.setObject(125);	//box.setObject(Integer.valueOf(125));
	}
	public static void main1(String[] args) {
		Box box = new Box();	//OK
	}
}
