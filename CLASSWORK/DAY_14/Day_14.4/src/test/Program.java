package test;

public class Program {
	public static void main(String[] args) {
		Integer i1 = new Integer(125);
		//int i2 = i1.intValue();	//UnBoxing
		int i2 = i1;	//Auto-UnBoxing
		System.out.println(i2);
	}
	public static void main1(String[] args) {
		int number = 10;
		Object obj = number;
		//Object obj = Integer.valueOf(number);
		System.out.println(obj);
	}
}
