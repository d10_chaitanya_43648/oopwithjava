package org.sunbeam.dac.launcher;

import java.util.Scanner;

import org.sunbeam.dac.model.Complex;

public class Program {
	private static void acceptRecord(Complex complex) {
		Scanner sc = new Scanner(System.in);
		if (complex != null) {
			System.out.print("Real Number	:	");
			complex.setReal(sc.nextInt());
			System.out.print("Imag Number	:	");
			complex.setImag(sc.nextInt());
		}
	}

	private static void printRecord(Complex complex) {
		if (complex != null) {
			System.out.println("Real Number	:	" + complex.getReal());
			System.out.println("Imag Number	:	" + complex.getImag());
		}
	}

	public static void main(String[] args) {

		Complex complex = new Complex();
		
		acceptRecord(complex);

		printRecord(complex);

	}
}
