package org.sunbeam.dac;

class Test{
	 int num1 = 10;
	 static int num2 = 20;
	public void printRecord( ) {
		System.out.println("Num1	:	"+this.num1);
		//System.out.println("Num2	:	"+this.num2); //OK
		System.out.println("Num2	:	"+Test.num2);
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test();
		t.printRecord();
	}
}