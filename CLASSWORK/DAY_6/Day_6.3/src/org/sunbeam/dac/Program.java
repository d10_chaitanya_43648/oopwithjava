package org.sunbeam.dac;

class Test{
	private Test( ) {
		System.out.println("Inside constructor");
	}
	public static Test getInstance( ) {
		Test instance = new Test( );
		return instance;
	}
}
public class Program {
	public static void main(String[] args) {
		//Test t = new Test( );	//Error : The constructor Test() is not visible
		Test t1 =  Test.getInstance();
		
		Test t2 =  Test.getInstance();
		
		Test t3 =  Test.getInstance();
	}
}
