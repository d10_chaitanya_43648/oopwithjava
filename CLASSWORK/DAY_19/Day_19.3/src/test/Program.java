package test;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;
public class Program {
	public static void main(String[] args) {
		Connection connection = null;
		Statement statement = null;
		try {
			
			Properties p = new Properties();
			InputStream inputStream = Program.class.getClassLoader().getResourceAsStream("config.properties");
			p.load(inputStream);
			
			Class.forName(p.getProperty("DRIVER"));
			
			connection = DriverManager.getConnection(p.getProperty("URL"), p.getProperty("USER"), p.getProperty("PASSWORD"));
			
 			statement = connection.createStatement();
			
 			String sql = "SELECT * FROM books";	//DQL
 			ResultSet rs = statement.executeQuery(sql);
 			
 			while( rs.next()) {
 				int bookId =  rs.getInt("book_id");
 				String subjectName = rs.getString("subject_name");
 				String bookName = rs.getString("book_name");
 				String authorName =  rs.getString("author_name");
 				float price = rs.getFloat("price");
 				System.out.printf("%-5d%-20s%-55s%-40s%-8.2f\n",bookId, subjectName, bookName, authorName, price);
 			}
 			rs.close();
		
		}catch( Exception ex ) {
			ex.printStackTrace();
		}finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
