//Step 1	:	Include DB connector into build path
package test;
//Step 2	:	import sql package
import java.sql.*;
public class Program {
	public static void main(String[] args) {
		Connection connection = null;
		Statement statement = null;
		try {
			//Step 3	:	Load and register driver
			Driver driver = new com.mysql.cj.jdbc.Driver();	//Upcasting
			DriverManager.registerDriver(driver);
			//Step 4	:	Establish connection using users credential
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dac_db", "sunbeam", "sunbeam");
			//Step 5	:	Create Statement/PreparedStatement/CallableStatement
 			statement = connection.createStatement();
			//Step 6	:	Prepare and execute query
 			String sql = "SELECT * FROM books";
 			ResultSet rs = statement.executeQuery(sql);
 			while( rs.next()) {
 				int bookId =  rs.getInt("book_id");
 				String subjectName = rs.getString("subject_name");
 				String bookName = rs.getString("book_name");
 				String authorName =  rs.getString("author_name");
 				float price = rs.getFloat("price");
 				System.out.printf("%-5d%-20s%-55s%-40s%-8.2f\n",bookId, subjectName, bookName, authorName, price);
 			}
 			rs.close();
		
		}catch( Exception ex ) {
			ex.printStackTrace();
		}finally {
			try {
				//Step 7	:	Close the resources
				statement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
