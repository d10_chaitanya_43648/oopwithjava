package test;

import java.sql.*;
import utils.DBUtil;

public class Program {
	public static void main(String[] args) {
		try (Connection connection = DBUtil.getConnection(); 
			 Statement statement = connection.createStatement();) {
			String sql = "UPDATE books SET price=574.50 WHERE book_id=1025"; // DML
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) updated");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void main3(String[] args) {
		try (Connection connection = DBUtil.getConnection(); 
			 Statement statement = connection.createStatement();) {
			String sql = "INSERT INTO books VALUES(1025,'OS','OS Concepts', 'Galvin', 100)"; // DML
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) updated");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void main2(String[] args) {
		try (Connection connection = DBUtil.getConnection(); 
			 Statement statement = connection.createStatement();) {
			String sql = "DELETE FROM books WHERE book_id=1025"; // DML
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) updated");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	public static void main1(String[] args) {
		try (Connection connection = DBUtil.getConnection(); 
			 Statement statement = connection.createStatement();) {
			String sql = "SELECT * FROM books"; // DQL
			try (ResultSet rs = statement.executeQuery(sql);) {
				while (rs.next())
					Program.processRow( rs );
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void processRow(ResultSet rs) throws Exception {
		int bookId = rs.getInt("book_id");
		String subjectName = rs.getString("subject_name");
		String bookName = rs.getString("book_name");
		String authorName = rs.getString("author_name");
		float price = rs.getFloat("price");
		System.out.printf("%-5d%-20s%-55s%-40s%-8.2f\n", bookId, subjectName, bookName, authorName, price);
	}
}
