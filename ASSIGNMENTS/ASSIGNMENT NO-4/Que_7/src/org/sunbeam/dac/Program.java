package org.sunbeam.dac;

class SavingsAccount {
	private static float annualInterestRate;
	private float savingsBalance;

	public float getSavingsBalance() {
		return savingsBalance;
	}

	static {
		annualInterestRate = 0.04f;
	}

	public SavingsAccount() {
		this.savingsBalance = 0.0f;
	}

	public SavingsAccount(float bal) {
		this.savingsBalance = bal;
	}

	public void calculateMonthlyInterest() {
		float cal = (this.savingsBalance * SavingsAccount.annualInterestRate / 12) + this.savingsBalance;
		this.savingsBalance = this.savingsBalance + (this.savingsBalance * SavingsAccount.annualInterestRate / 12);
	}

	public static void modifyInterestRate() {
		SavingsAccount.annualInterestRate = 0.05f;
	}

}

public class Program {

	public static void main(String[] args) {

		SavingsAccount saver1 = new SavingsAccount(2000.0f);
		SavingsAccount saver2 = new SavingsAccount(3000.0f);

		System.out.println("The monthly interest for each of 12 months : ");
		for (int i = 0; i < 12; i++) 
			saver1.calculateMonthlyInterest();
		System.out.println("New Balance of saver1 after 12 months with interest : " + saver1.getSavingsBalance());
		
		for (int i = 0; i < 12; i++)
			saver2.calculateMonthlyInterest();
		System.out.println("New Balance of saver1 after 12 months with interest : " + saver2.getSavingsBalance());
		System.out.println("----------------------------------------------------------------------------------");
		
		SavingsAccount.modifyInterestRate();
		saver1.calculateMonthlyInterest();	
		System.out.println("The next month�s interest of saver1 : "+saver1.getSavingsBalance());
		
		saver2.calculateMonthlyInterest();	
		System.out.println("The next month�s interest of saver2 : "+saver2.getSavingsBalance());
		System.out.println("===================================================================================");
		
	}
}
