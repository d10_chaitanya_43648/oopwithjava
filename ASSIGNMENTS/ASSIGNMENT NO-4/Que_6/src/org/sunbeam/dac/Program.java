package org.sunbeam.dac;

import java.util.Scanner;

public class Program {
	private static Scanner sc = new Scanner(System.in);

	public static void unitedStatesDoller(double money) {
		System.out.printf("Your new Currency : %.2f", (float)money * 0.014f);
	}

	public static void egyptainPound(double money) {
		System.out.printf("Your new Currency : %.2f", (float)money * 0.21);
	}

	public static void euro(double money) {
		System.out.printf("Your new Currency : %.2f", (float)money * 0.011);
	}

	public static int menu() {
		int choice;
		System.out.println("In which type You want to convert your money???");
		System.out.println("0. Exit");
		System.out.println("1. United States Doller");
		System.out.println("2. Egyptain Pound");
		System.out.println("3. Euro");
		System.out.print("Enter Choice :");
		choice = sc.nextInt();
		return choice;
	}

	public static void main(String[] args) {
		int choice;
		do {
			choice = Program.menu();
			if(choice == 0)
				break;
			double moneyInput;
			System.out.print("Enter Your Money : ");
			moneyInput = sc.nextDouble();
			
			switch (choice) {
			case 1:
				Program.unitedStatesDoller(moneyInput);
				break;
			case 2:
				Program.egyptainPound(moneyInput);
				break;
			case 3:
				Program.euro(moneyInput);
				break;
			}
			System.out.println();
		} while (choice != 0);

	}
}
