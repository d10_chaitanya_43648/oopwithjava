package org.sunbeam.dac;

enum TrafficLight{
	RED(10), GREEN(20), YELLOW(5);
	private int duration;

	private TrafficLight(int duration) {
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	
}
public class Program {

	public static void main(String[] args) {
		TrafficLight[] light = TrafficLight.values();
		for(TrafficLight lt : light) {
			System.out.println(lt.name()+"		Duration : "+lt.getDuration()+" Seconds");
		}
	}

}
