package org.sunbeam.dac;

import java.util.Scanner;

class Rational {
    private int numerator;
    private int denominator;

    public Rational() {
        numerator = 1;
        denominator = 1;
    }
    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }
    public int getNumerator() {
        return numerator;
    }
    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }
    public int getDenominator() {
        return denominator;
    }
    public void setDenominator(int denominator) {
        this.denominator = denominator;
    }
    public Rational add(Rational r) {
        int  num = this.numerator * r.denominator + r.numerator * this.denominator;
        int denom = this.denominator * r.denominator;
        Rational rat = new Rational(num, denom);
        return rat;
    }
    public Rational subtract(Rational r) {
        int num = this.numerator * r.denominator - r.numerator * this.denominator;
        int denom = this.denominator  * r.denominator;
        Rational rat = new Rational(num, denom);
        return rat;
    }
    public Rational multiply(Rational r) {
        int num = this.numerator * r.numerator;
        int denom = this.denominator * r.denominator;
        Rational rat = new Rational(num, denom);
        return rat;
    }
    public Rational divide(Rational r) {
        int num = this.numerator / r.numerator;
        int denom = this.denominator / r.denominator;
        Rational rat = new Rational(num, denom);
        return rat;
    }
    public String print() {
        String str = "(" + numerator + "/" + denominator + ")";
        return str;
    }
    public static int menu() {
    	int choice;
    	System.out.println("0. Exit");
    	System.out.println("1. Addition");
    	System.out.println("2. Subtraction");
    	System.out.println("3. Multiplication");
    	System.out.println("4. Division");
    	 Scanner sc = new Scanner(System.in);
    	 choice = sc.nextInt();
    	 return choice;
    }
}
public class Test {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
	        Rational obj1;
	        Rational obj2;



	        int choice = 1;
	        do {
	        	choice = Rational.menu();
	        	if(choice == 0)
	        		break;
		        System.out.println("Enter numerator for the first rational number: ");
		        int numerator1 = sc.nextInt();
		        System.out.println("Enter a non-zero denominator for the first rational number: ");
		        int denominator1 = sc.nextInt();
		        System.out.println("Enter a numerator for the second rational number: ");
		        int numerator2 = sc.nextInt();
		        System.out.println("Enter a non-zero denominator for the second rational number: ");
		        int denominator2 = sc.nextInt();

		        obj1 = new Rational(numerator1, denominator1);
		        obj2 = new Rational(numerator2, denominator2);
	        	
	        	
	        	
	        	
		        System.out.println("First rational number is: " + obj1.print());
		        System.out.println("Second rational number is: " + obj2.print());

	        	switch(choice) {
	        	case 1:
	    	        System.out.println("Addition of the rational number is: " + obj1.add(obj2).print());
	        		break;
	        	case 2:
	    	        System.out.println("Subtraction of the rational number is: " + obj1.subtract(obj2).print());
	        		break;
	        	case 3:
	    	        System.out.println("Multiplication of the rational number is: " + obj1.multiply(obj2).print());
	        		break;
	        	case 4:
	    	        System.out.println("Division of the rational number is: " + obj1.divide(obj2).print());
	        		break;	        	
	        	}
	        	System.out.println("----------------------------------------------------------------------------");
	        }while(choice != 0);

	}

}


