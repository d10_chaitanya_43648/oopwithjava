package org.sunbeam.dac;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	private static int cnt1;
	private static int cnt2;
	private static int index;

	static {
		cnt1 = 1;
		cnt2 = 6;
		index = 0;
	}

	public static void firstClass(boolean[] arr) {
		System.out.println("First Class...");
		for (index = cnt1 - 1; index <= arr.length; ++index) {
			if (arr[index] == false) {
				arr[index] = true;
				System.out.println("Seat no. : " + cnt1);
				cnt1++;
				// System.out.println("cnt1 : "+cnt1);
				break;
			}
		}
	}

	public static void economyClass(boolean[] arr) {
		System.out.println("Economy Class...");
		for (index = cnt2 - 1; index <= arr.length; ++index) {
			if (arr[index] == false) {
				arr[index] = true;
				System.out.println("Seat no. : " + cnt2);
				cnt2++;
				// System.out.println("cnt2 : "+cnt2);
				break;
			}
		}
	}

	public static void main(String[] args) {
		boolean[] arr = new boolean[10];

		System.out.println(Arrays.toString(arr));
		String str1, str2;
		int type;
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Select Type : ");
			type = sc.nextInt();

			//System.out.println("Selected Type : " + type);

			if ((cnt1 <= 5) && (cnt2 <= 10)) {
				if (type == 1)
					Program.firstClass(arr);

				else if (type == 2)
					Program.economyClass(arr);
			} else if ((cnt1 <= 5) && (cnt2 > 10)) {
				if (type == 1) {
					Program.firstClass(arr);
				}else {
				System.out.println("Economy Class is full..");
				System.out.print("Is it acceptable to be placed in the first-class section?(Yes/No) : ");
				sc.nextLine();
				str1 = sc.nextLine();
				if (str1.equalsIgnoreCase("yes")) {
					Program.firstClass(arr);
				} else
					System.out.println("Next flight leaves in 3 hours...");
				}
			} else if ((cnt2 <= 10) && (cnt1 > 5)) {
				if (type == 2) {
					Program.economyClass(arr);
				}else {
				System.out.println("First Class is full..");
				System.out.print("Is it acceptable to be placed in the economy-class section?(Yes/No) : ");
				sc.nextLine();
				str2 = sc.nextLine();
				if (str2.equalsIgnoreCase("yes"))
					Program.economyClass(arr);
				else
					System.out.println("Next flight leaves in 3 hours...");
				}
			} else
				System.out.println("Next flight leaves in 3 hours...");
			System.out.println(Arrays.toString(arr));
			System.out.println("------------------------------------------------------------------------------------");
			System.out.println();

		}
	}
}
