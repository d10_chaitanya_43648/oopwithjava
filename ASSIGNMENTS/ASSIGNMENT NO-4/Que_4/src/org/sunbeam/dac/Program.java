package org.sunbeam.dac;
import java.util.Scanner;

class Stack{
	
	private int[] arr = new int[5];
	private int top;
	static Scanner sc = new Scanner(System.in);

	public Stack() {
		this.top = -1;
	}

	public static int menu() {
		int choice;
		System.out.println("Static Stack");
		System.out.println("0. Exit");
		System.out.println("1. Push");
		System.out.println("2. Pop");
		System.out.println("3. Peek");
		System.out.print("Enter Choice : ");
		choice = sc.nextInt();
		return choice;
	}

	public void pop_element()
	{
		//step2: decrement the value of top by 1, i.e. we are deleting an ele from the stack
		this.top--;
	}
	
	public void push_element( int ele)
	{
		//step2: increment the value of top by 1
		this.top++;

		//step3: insert/add ele onto the at top position/end
		this.arr[ this.top ] = ele;
	}

	public int peek_element()
	{
		//step2: return the value of an ele which is at top position/end
		//(without incrementing/decrementing value of top
		return ( this.arr[ this.top ] );
	}
	
	public boolean is_stack_empty()
	{
		if( this.top == -1 )
			return true;//return true;
		else
			return false; //return false
	}
	
	public boolean is_stack_full()
	{
		if( this.top == (5-1) )
			return true;//return true;
		else
			return false; //return false
	}
	
	
	
}
public class Program {
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		Stack s = new Stack();
			
			int ele, choice;

			do
			{
				choice = Stack.menu();
				switch(choice)
				{
				case 1:
					//step1: check stack is not full
					if( s.is_stack_full() != true)
					{
						System.out.print("Enter an ele: ");
						ele = sc.nextInt();
						s.push_element(ele);
						System.out.println(ele+" is pushed into stack ....");
					}
					else
						System.out.println("Stack overflow !!!");
					break;

				case 2:
					//step1: check stack is not empty
					if( s.is_stack_empty() != true)
					{
						ele = s.peek_element();
						s.pop_element();
						System.out.println("Popped ele is: "+ele);
					}
					else
						System.out.println("Stack underflow !!!");
					break;

				case 3:
					//step1: check stack is not empty
					if( s.is_stack_empty() != true)
					{
						ele = s.peek_element();
						System.out.println("Topmost ele is: "+ele);;
					}
					else
						System.out.println("Stack underflow !!!");
					break;
				}
			}while(choice != 0);


		}
	}
