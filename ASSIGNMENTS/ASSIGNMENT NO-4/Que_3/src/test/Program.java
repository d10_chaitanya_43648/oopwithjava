package test;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		float[][] arr = new float[4][5];

		int salespersonNumber;
		int productNumber;
		int con = 1;

		while (con == 1) {
			System.out.println("Each Salesperson Enter Maximun 5 Products for each day :");
			System.out.print("Enter Salesperson Number : ");
			salespersonNumber = sc.nextInt();

			System.out.print("Enter Product Number : ");
			productNumber = sc.nextInt();

			System.out.print("Enter Value of Product : ");
			arr[salespersonNumber - 1][productNumber - 1] = sc.nextFloat();

			System.out.print("If You want to continue then enter 1 otherwise enter 0 : ");
			con = sc.nextInt();
		}
		
		
		System.out.println();
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("Product		Sp-1	Sp-2	Sp-3	Sp-4	Total sales of each product");
		System.out.println("--------------------------------------------------------------------------");

		for(int index = 0; index < 5; index++) {
				System.out.printf("Product"+(index+1)+"	"+arr[0][index]+"	"+arr[1][index]+"	"+arr[2][index]+"	"+arr[3][index]+"	"+(arr[0][index] + arr[1][index] + arr[2][index] + arr[3][index] ));
				System.out.println();					
		}

		float sum1 = arr[0][0] + arr[0][1] + arr[0][2]+ arr[0][3] +arr[0][4]; 
		float sum2 = arr[1][0] + arr[1][1] + arr[1][2]+ arr[1][3] +arr[1][4];
		float sum3 = arr[2][0] + arr[2][1] + arr[2][2]+ arr[2][3] +arr[2][4];
		float sum4 = arr[3][0] + arr[3][1] + arr[3][2]+ arr[3][3] +arr[3][4];
		float sum5 = sum1 + sum2 +sum3+ sum4;
		System.out.println("--------------------------------------------------------------------------");
		System.out.println("Total : 	"+sum1+"	"+sum2+"	"+sum3+"	"+sum4+"	"+sum5);
		System.out.println("--------------------------------------------------------------------------");
	}

}