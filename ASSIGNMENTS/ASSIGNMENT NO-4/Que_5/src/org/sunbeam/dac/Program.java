package org.sunbeam.dac;
import java.util.Scanner;

class Queue{
	
	private int[] arr = new int[5];
	private int rear;
	private int front;

	static Scanner sc = new Scanner(System.in);

	public Queue() {
		this.front = -1;
		this.rear = -1;
	}

	public static int menu() {
		int choice;
		System.out.println("Linear Queue : ");
		System.out.println("0. Exit");
		System.out.println("1. enqueue");
		System.out.println("2. dequeue");
		System.out.print("Enter Choice : ");
		choice = sc.nextInt();
		return choice;
	}

	public void dequeue()
	{
		//step2: increment the value of front by i.e. we are deleting ele from the queue
		this.front++;
	}

	public int get_front()
	{
		//return the value of an ele which is at front position
		return ( this.arr[ this.front ] );
	}

	public void enqueue(int ele)
	{
		//step2: increment the value of rear by 1
		this.rear++;
		//step3: insert an ele into the queue at rear position
		this.arr[ this.rear ]  = ele;
		//step4: if( front == -1 ) ==> front = 0
		if( this.front == -1 )
			this.front = 0;
	}
	
	public boolean is_queue_full()
	{
		return ( this.rear == (5-1) );
	}

	public boolean is_queue_empty()
	{
		return (this.rear == -1 || this.front > this.rear );
	}
	
}
public class Program {
	
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);

		Queue q = new Queue();
			
			int ele, choice;

			do
			{
				choice = Queue.menu();
				switch(choice)
				{
				case 1:
					//step1: check queue is not full
					if( q.is_queue_full() != true )
					{
						System.out.println("Enter ele: ");
						ele = sc.nextInt();
						q.enqueue(ele);
						System.out.println(ele+" inserted into the queue...");
					}
					else
						System.out.println("queue is full !!!");
					break;

				case 2:
					//step1: check queue is not empty
					if( q.is_queue_empty() != true)
					{
						ele = q.get_front();
						q.dequeue();
						System.out.println("Deleted ele is: "+ele);
					}
					else
						System.out.println("Queue is empty !!!");
					break;
				}
			}while(choice != 0);


		}
	}
