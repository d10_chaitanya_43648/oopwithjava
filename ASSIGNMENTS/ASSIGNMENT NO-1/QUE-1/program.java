class program{
    public static void main (String[] args){

        System.out.println("INTEGER range: min:"+Integer.MIN_VALUE+ "  max:" +Integer.MAX_VALUE);
        System.out.println("DOUBLE range: min:"+Double.MIN_VALUE+ "  max:" +Double.MAX_VALUE);
        System.out.println("LONG RANGE range: min:"+Long.MIN_VALUE+ "  max:" +Long.MAX_VALUE);
        System.out.println("SHORT RANGE range: min:"+Short.MIN_VALUE+ "  max:" +Short.MAX_VALUE);
        System.out.println("BYTE RANGE range: min:"+Byte.MIN_VALUE+ "  max:" +Byte.MAX_VALUE);
        System.out.println("FLOAT RANGE range: min:"+Float.MIN_VALUE+ "  max:" +Float.MAX_VALUE);
    }
}