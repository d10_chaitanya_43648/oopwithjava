class program{
    public static void main ( String[] args){

        int number=20;
        String str = Integer.toBinaryString(number);
        System.out.println("Binary equivalent : "+str);
        str = Integer.toOctalString(number);
        System.out.println("Octal equivalent : "+str);
        str = Integer.toHexString(number);
        System.out.println("Hexadecimal equivalent : "+str); 
    }
}