package org.sunbeam.dac;

class Employee{
	private String FName;
	private String LName;
	private double Monthly_salary;
	
	public Employee() {
		FName = null;
		LName = null;
		Monthly_salary = 0.0;
	}

	public String getFName() {
		return FName;
	}

	public void setFName(String fName) {
		FName = fName;
	}

	public String getLName() {
		return LName;
	}

	public void setLName(String lName) {
		LName = lName;
	}

	public double getMonthly_salary() {
		return Monthly_salary;
	}

	public void setMonthly_salary(double monthly_salary) {
		Monthly_salary = monthly_salary;
	}
	
	public void yearly_salary(double sal){
		 System.out.println("Yearly Salary : "+sal*12);
	}
	
}

public class EmployeeTest {

	public static void main(String[] args) {
		Employee emp1 = new Employee();
		
		Employee emp2 = new Employee();

		emp1.setFName("Nikhil");
		emp1.setLName("Mahajan");
		emp1.setMonthly_salary(15000);
		
		emp2.setFName("Lokesh");
		emp2.setLName("Patil");
		emp2.setMonthly_salary(20000);
		
		System.out.println("Name : "+emp1.getFName()+" "+emp1.getLName()+" 		Salary : "+emp1.getMonthly_salary());

		System.out.println("Name : "+emp2.getFName()+" "+emp2.getLName()+" 		Salary : "+emp2.getMonthly_salary());

		System.out.println();
		emp1.yearly_salary(emp1.getMonthly_salary());
		emp2.yearly_salary(emp2.getMonthly_salary());

		System.out.println();
		System.out.println("Rise salary by 10% : ");
		
		emp1.setMonthly_salary(emp1.getMonthly_salary() * 1.10);
		emp2.setMonthly_salary(emp2.getMonthly_salary() * 1.10);

		emp1.yearly_salary(emp1.getMonthly_salary());
		emp2.yearly_salary(emp2.getMonthly_salary());
	}

}
