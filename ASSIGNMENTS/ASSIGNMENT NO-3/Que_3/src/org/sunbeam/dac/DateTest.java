package org.sunbeam.dac;

class Date{
	private int month;
	private int day;
	private int year;
	
	public Date(){
		month = 1;
		day = 1;
		year = 1;
	}
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public void displayDate(){
		System.out.println(month+" / "+day+" / "+year);
	}
}

public class DateTest {

	public static void main(String[] args) {
		Date dt = new Date();
		
		dt.setMonth(11);
		dt.setDay(25);
		dt.setYear(2020);
		
		dt.displayDate();

	}

}
