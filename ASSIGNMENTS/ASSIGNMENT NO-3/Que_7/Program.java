import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter the radius of Circle : ");
        int radius = sc.nextInt();

        float diameter = 2 * radius;
        float circumference = 2 * (float)Math.PI * radius;
        float area = (float)Math.PI * radius * radius;

        System.out.println("The diameter of Circle : "+diameter);
        System.out.println("The circumference of Circle : "+circumference);
        System.out.println("The area of Circle : "+area);
    }
}