package org.sunbeam.dac.model;

public class ItemInfo {

	private int item;
	private float value;
	public ItemInfo(int item, float value) {
		this.item = item;
		this.value = value;
	}
	public int getItem() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	
}
