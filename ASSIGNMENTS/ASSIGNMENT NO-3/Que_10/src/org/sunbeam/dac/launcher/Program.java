package org.sunbeam.dac.launcher;

import java.util.Scanner;

import org.sunbeam.dac.model.ItemInfo;

public class Program {
	
	public static void main(String[] args) {
		int item_no = 1;
		float calculate = 0;
		float earnings = 0;
		ItemInfo item1 = new ItemInfo(1, 239.99f);
		ItemInfo item2 = new ItemInfo(2, 129.75f);
		ItemInfo item3 = new ItemInfo(3, 99.95f);
		ItemInfo item4 = new ItemInfo(4, 350.89f);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enters salespersonís items sold for last week (item no.) : ");
		
		while(item_no != 0) {

			item_no = sc.nextInt();

			if(item_no > 4)
				System.out.println("Enter Valid Item...");
			if(item_no == item1.getItem())
				calculate = calculate + item1.getValue();
			if(item_no == item2.getItem())
				calculate = calculate + item2.getValue();
			if(item_no == item3.getItem())
				calculate = calculate + item3.getValue();
			if(item_no == item4.getItem())
				calculate = calculate + item4.getValue();	
		}	
		
		if(calculate != 0)
		earnings = 200 + 0.09f * calculate ;
		System.out.println("Total Earnings of salesperson is  : $"+earnings);
		
		
	}
}
