import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Amount to Deposit : ");
        float amount = sc.nextFloat();
        System.out.print("Enter Number of Year for Amount to Deposit : ");
        int year = sc.nextInt();

        float compound, rate;
        rate = (year >= 5) ? 10 : (((amount < 2000) && (year >= 2)) ? 5 : (((amount >= 2000) && (amount < 6000) && (year >= 2)) ? 7 : (((amount >= 6000) && (year >= 1)) ? 8 : 3)));
        
        compound = amount * (float)Math.pow((1 + (rate/100)), year);

        System.out.println("Money in the customer's account at the end of the "+year+" is : "+compound);
    }
}