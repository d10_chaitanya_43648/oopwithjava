package org.sunbeam.dac.launcher;

import org.sunbeam.dac.model.HealthProfile;

public class Program {

	public static void main(String[] args) {
		HealthProfile hr = new HealthProfile("Nikhil", "Mahajan", 29, 03, 1997, "Male", 72, 185.45);
		
		System.out.println("First Name : "+hr.getFname());
		System.out.println("Last Name : "+hr.getLname());
		System.out.println("Gender : "+hr.getGender());
		System.out.println("Date of Birth : "+hr.getBd().getDay()+" / "+hr.getBd().getMonth()+" / "+hr.getBd().getYear());
		System.out.println("Height (in inches) : "+hr.getHeight());
		System.out.println("Weight (pounds) : "+hr.getWeight());

		System.out.println();
		int age_diff = hr.age_calculator(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());
		System.out.println("Person�s age (in years) 			: "+age_diff); 
	
		double BMI = hr.bmi(hr.getWeight(), hr.getHeight());
		System.out.printf("BMI 						: %.2f\n",BMI);
		
		int Max_hr = hr.max_heart_rate(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());
		System.out.println("Person�s maximum heart rate in beats per minute	: "+Max_hr); 
		
		hr.target_heart_rate(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());


	}

}
