package org.sunbeam.dac.model;

import java.time.LocalDate;
import java.time.Period;


public class HealthProfile {

	private String Fname;
	private String Lname;
	private BirthDate bd;
	private double height;
	private double weight;
	private String gender;
	
	public HealthProfile(String first, String last, int d, int m, int y, String gender, double height, double weight){
		this.Fname = first;
		this.Lname = last;
		this.height = height;
		this.weight = weight;
		this.gender = gender;
		this.bd = new BirthDate(d, m, y);
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getFname() {
		return Fname;
	}

	public void setFname(String fname) {
		Fname = fname;
	}

	public String getLname() {
		return Lname;
	}

	public void setLname(String lname) {
		Lname = lname;
	}

	public BirthDate getBd() {
		return bd;
	}

	public void setBd(BirthDate bd) {
		this.bd = bd;
	}
	
	public int age_calculator(int year, int month, int dayOfMonth) {
		LocalDate now = LocalDate.of(year, month, dayOfMonth);
		LocalDate now1 = LocalDate.now();

		Period xyz = Period.between(now, now1);
		int diff = xyz.getYears();
		return diff;
	}
	
	public double bmi(double w, double h) {
		double w_in_kg = 0.454 * w;
		double h_in_meter = 0.0254 * h;
		double bmi = w_in_kg / (h_in_meter * h_in_meter);
		return bmi;
	}
	
	public int max_heart_rate(int year, int month, int dayOfMonth) {
		int age_diff = age_calculator(year, month, dayOfMonth);
		int mhr = 220 - age_diff;
		return mhr;
	}
	
	public void target_heart_rate(int year, int month, int dayOfMonth){
		// 50�85% mhr
		int max_rate = max_heart_rate(year, month, dayOfMonth);
		System.out.println("Person Target Heart Rate Range 			: "+0.50*max_rate+" - "+0.85*max_rate);
	}
	
}
