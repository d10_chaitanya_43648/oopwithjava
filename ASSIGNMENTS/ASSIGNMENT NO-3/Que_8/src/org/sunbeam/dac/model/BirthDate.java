package org.sunbeam.dac.model;

public class BirthDate {

	private int day;
	private int month;
	private int year;
	public BirthDate() {
		
	}
	public BirthDate(int d1, int m1, int y1) {
		this.day = d1;
		this.month = m1;
		this.year = y1;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
}
