package org.sunbeam.dac;

import java.time.LocalDate;
import java.time.Period; 

class birthdate{
	private int day;
	private int month;
	private int year;
	public birthdate() {
		
	}
	public birthdate(int d1, int m1, int y1) {
		this.day = d1;
		this.month = m1;
		this.year = y1;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}

class HeartRates{
	private String Fname;
	private String Lname;
	private birthdate bd;
	
	public HeartRates(String first, String last, int d, int m, int y){
		this.Fname = first;
		this.Lname = last;
		this.bd = new birthdate(d, m, y);
	}

	public String getFname() {
		return Fname;
	}

	public void setFname(String fname) {
		Fname = fname;
	}

	public String getLname() {
		return Lname;
	}

	public void setLname(String lname) {
		Lname = lname;
	}

	public birthdate getBd() {
		return bd;
	}

	public void setBd(birthdate bd) {
		this.bd = bd;
	}
	
	public int age_calculator(int year, int month, int dayOfMonth) {
		LocalDate now = LocalDate.of(year, month, dayOfMonth);
		LocalDate now1 = LocalDate.now();

		Period xyz = Period.between(now, now1);
		int diff = xyz.getYears();
		return diff;
	}
	
	public int max_heart_rate(int year, int month, int dayOfMonth) {
		int age_diff = age_calculator(year, month, dayOfMonth);
		int mhr = 220 - age_diff;
		return mhr;
	}
	
	public void target_heart_rate(int year, int month, int dayOfMonth){
		// 50�85% mhr
		int max_rate = max_heart_rate(year, month, dayOfMonth);
		System.out.println("Person Target Heart Rate Range 			: "+0.50*max_rate+" - "+0.85*max_rate);
	}
 
}

public class TargetHeartRate {

	public static void main(String[] args) {
		
		HeartRates hr = new HeartRates("Nikhil", "Mahajan", 29, 03, 1997);
		
		System.out.println("First Name : "+hr.getFname());
		System.out.println("Last Name : "+hr.getLname());
		System.out.println("Date of Birth : "+hr.getBd().getDay()+" / "+hr.getBd().getMonth()+" / "+hr.getBd().getYear());

		System.out.println();
		int age_diff = hr.age_calculator(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());
		System.out.println("Person�s age (in years) 			: "+age_diff); 
	
		int Max_hr = hr.max_heart_rate(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());
		System.out.println("Person�s maximum heart rate in beats per minute	: "+Max_hr); 
		
		hr.target_heart_rate(hr.getBd().getYear(), hr.getBd().getMonth(), hr.getBd().getDay());

	}

}
