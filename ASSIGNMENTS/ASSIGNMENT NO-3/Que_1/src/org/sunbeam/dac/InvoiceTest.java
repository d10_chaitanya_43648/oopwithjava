package org.sunbeam.dac;

class Invoice{
	private String part_number;
	private String part_description;
	private int Qty;
	private double price_per_item;
	
	public Invoice() {
		part_number = null;
		part_description = null;
		Qty = 0;
		price_per_item = 0.0;
	}
	
	public String getPart_number() {
		return part_number;
	}
	public void setPart_number(String part_number) {
		this.part_number = part_number;
	}
	public String getPart_description() {
		return part_description;
	}
	public void setPart_description(String part_description) {
		this.part_description = part_description;
	}
	public int getQty() {
		return Qty;
	}
	public void setQty(int qty) {
		Qty = qty;
	}
	public double getPrice_per_item() {
		return price_per_item;
	}
	public void setPrice_per_item(double price_per_item) {
		this.price_per_item = price_per_item;
	}
	
	public double getInvoiceAmount(int Q, double price){
		if( Q < 0)
			Q = 0;
		if( price < 0.0 )
			price = 0.0;
		return (double)Q * price;
	}
}

public class InvoiceTest {

	public static void main(String[] args) {
		Invoice item = new Invoice();
		
		double num;
		
		item.setPart_number("1");
		item.setPart_description("Pipe");
		item.setQty(20);
		item.setPrice_per_item(180);
		
		System.out.println("Part Number : "+item.getPart_number());
		System.out.println("Part Description : "+item.getPart_description());
		System.out.println("Qty : "+item.getQty());
		System.out.println("Price per item : "+item.getPrice_per_item());
		
		num = item.getInvoiceAmount(item.getQty(), item.getPrice_per_item());
		
		System.out.println();
		System.out.println("The invoice amount : "+num);
	}

}
