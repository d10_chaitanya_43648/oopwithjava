package org.sunbeam.dac.model;

public class PlanInfo {

	private String Plan;
	private double MonthlyCharges;
	private double CallCharges;
	private double SMSCharges;
	private int Freecalls;
	private int DiscountCalls;
	
	public PlanInfo() {
		this(null, 0, 0, 0, 0, 0);
	}
	
	public PlanInfo(String plan, double monthlyCharges, double callCharges, double sMSCharges, int freecalls,
			int discountCalls) {
		Plan = plan;
		MonthlyCharges = monthlyCharges;
		CallCharges = callCharges;
		SMSCharges = sMSCharges;
		Freecalls = freecalls;
		DiscountCalls = discountCalls;
	}

	public String getPlan() {
		return Plan;
	}

	public void setPlan(String plan) {
		Plan = plan;
	}

	public double getMonthlyCharges() {
		return MonthlyCharges;
	}

	public void setMonthlyCharges(double monthlyCharges) {
		MonthlyCharges = monthlyCharges;
	}

	public double getCallCharges() {
		return CallCharges;
	}

	public void setCallCharges(double callCharges) {
		CallCharges = callCharges;
	}

	public double getSMSCharges() {
		return SMSCharges;
	}

	public void setSMSCharges(double sMSCharges) {
		SMSCharges = sMSCharges;
	}

	public int getFreecalls() {
		return Freecalls;
	}

	public void setFreecalls(int freecalls) {
		Freecalls = freecalls;
	}

	public int getDiscountCalls() {
		return DiscountCalls;
	}

	public void setDiscountCalls(int discountCalls) {
		DiscountCalls = discountCalls;
	}
	
	
}
