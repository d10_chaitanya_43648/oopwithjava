package org.sunbeam.dac.launcher;

import org.sunbeam.dac.model.PlanInfo;

public class BillCalculate {
	
public void BillCalculator(PlanInfo plan, int CMade, int SSent){
		
		System.out.println();
		System.out.println("###Bill Invoice : ");
		System.out.println("--------------------------------------------------------------------------------------");
		System.out.println("Items    		Amount    			Explanation");
		System.out.println("--------------------------------------------------------------------------------------");

		double FCallCharges, NCallCharges, RCallCharges;
		double SmsBill, TotalCharges, FinalBill;
		
		FCallCharges = 0;
		System.out.println("First "+plan.getFreecalls()+" Calls		"+FCallCharges+"		[(Free calls)]");
		System.out.println("--------------------------------------------------------------------------------------");

		NCallCharges = plan.getDiscountCalls() * plan.getCallCharges() * 0.50 ;
		System.out.println("Next "+plan.getDiscountCalls()+" Calls		"+NCallCharges+"		[i.e. "+plan.getDiscountCalls()+" * "+0.50 * plan.getCallCharges()+" (50 % Discount) ]");
		System.out.println("--------------------------------------------------------------------------------------");

		RCallCharges = (CMade - plan.getFreecalls()- plan.getDiscountCalls()) * plan.getCallCharges();
		System.out.println("Remaining "+(CMade - plan.getFreecalls()- plan.getDiscountCalls())+" Calls	"+RCallCharges+"		[i.e. "+(CMade - plan.getFreecalls()- plan.getDiscountCalls())+" * "+plan.getCallCharges()+" (Regular Billing)]");
		System.out.println("--------------------------------------------------------------------------------------");

		SmsBill = SSent * plan.getSMSCharges();
		System.out.println("SMS Bill 		"+SmsBill+"		[i.e. "+SSent+" * "+plan.getSMSCharges()+" (Regular Billing)]");
		System.out.println("--------------------------------------------------------------------------------------");

		TotalCharges = FCallCharges + NCallCharges + RCallCharges + SmsBill + plan.getMonthlyCharges();
		System.out.println("Total Charges 		"+TotalCharges+"		[i.e. Monthly Charges + Bill Charges]");
		System.out.println("--------------------------------------------------------------------------------------");

		FinalBill = TotalCharges + TotalCharges * 0.125; 
		System.out.println("Final Bill 		"+FinalBill+"	[i.e. Total Charges + Service Tax 12.5%]");
		System.out.println("--------------------------------------------------------------------------------------");

		
	}
	
}
