package org.sunbeam.dac.launcher;

import java.util.Scanner;

import org.sunbeam.dac.model.PlanInfo;


public class Bill {
	

	public static void main(String[] args) {
		PlanInfo planA = new PlanInfo("PLAN-A", 199, 1.00, 1.00, 50, 50);
		PlanInfo planB = new PlanInfo("PLAN-B", 299, 0.80, 0.75, 75, 75);
		PlanInfo planC = new PlanInfo("PLAN-C", 399, 0.60, 0.50, 100, 100);

		try (Scanner sc = new Scanner(System.in)) {
			BillCalculate bc = new BillCalculate();
			
			System.out.print("Enter Plan which you want : ");
			String userplan = sc.nextLine();
			System.out.print("Enter Number of calls done/made : ");
			int callsmade = sc.nextInt();
			System.out.print("Enter Number of SMS sent : ");
			int sentsms = sc.nextInt();
			
			if(userplan.equalsIgnoreCase("PLAN-A"))
				bc.BillCalculator(planA, callsmade, sentsms);
			
			if(userplan.equalsIgnoreCase("PLAN-B"))
				bc.BillCalculator(planB, callsmade, sentsms);
				
			if(userplan.equalsIgnoreCase("PLAN-C"))
				bc.BillCalculator(planC, callsmade, sentsms);
		}

	
	}

}
