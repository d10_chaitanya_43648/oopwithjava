import java.util.Scanner;

class Program{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        int account, balance, charges, credits, credit_limit, newbal;
        int choice;
        System.out.println("0. Exit");
        System.out.println("1. Continue");
        System.out.print("Enter Choice : ");
        choice = sc.nextInt();  

        while(choice == 1)
        {
            System.out.print("Enter Account Number: ");
            account = sc.nextInt();          
            System.out.print("Enter Beginning Balance: ");
            balance = sc.nextInt();
            System.out.print("Enter Total Charges: ");
            charges = sc.nextInt();
            System.out.print("Enter Total Credits: ");
            credits = sc.nextInt();
            System.out.print("Enter Credit Limit: ");
            credit_limit = sc.nextInt();

            newbal = balance + charges - credits;
            System.out.println("New Balance : " + newbal);
            if ( newbal > credit_limit)
            {
                System.out.println("Credit Limit Exceeded");
                break;
            }
        }
    }
}
