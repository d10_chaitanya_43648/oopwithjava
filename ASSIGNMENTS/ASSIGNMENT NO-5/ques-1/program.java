package org.sunbeam.dac;

abstract class Employee {
	private String firstName;
	private String lastName;
	private int socialSecurityNumber;

	public Employee(String firstName, String lastName, int socialSecurityNumber) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public Employee() {
		this("", "", 0);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(int socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public abstract float earning();

	public String toString() {
		return String.format("First Name : %s\nLast Name : %s\nSocial Seurity Number : %d\n", this.firstName,
				this.lastName, this.socialSecurityNumber);
	}

}

class SalariedEmployee extends Employee {
	private float weeklySalary;

	public SalariedEmployee() {
		super();
	}

	public SalariedEmployee(String firstName, String lastName, int socialSecurityNumber, float weeklySalary) {
		super(firstName, lastName, socialSecurityNumber);
		this.weeklySalary = weeklySalary;
	}

	public float getWeeklySalary() {
		return weeklySalary;
	}

	public void setWeeklySalary(float weeklySalary) {
		this.weeklySalary = weeklySalary;
	}

	public float earning() {
		return weeklySalary;
	}

	public String toString() {
		return String.format("%sEarning of Salaries Employee : %.2f", super.toString(), this.earning());
	}
}

class HourlyEmployee extends Employee {
	private float hourlyWage;
	private float hoursWorked;

	public HourlyEmployee() {
		super();
	}

	public HourlyEmployee(String firstName, String lastName, int socialSecurityNumber, float hw, float hwd) {
		super(firstName, lastName, socialSecurityNumber);
		this.hourlyWage = hw;
		this.hoursWorked = hwd;
	}

	public float getHourlyWage() {
		return hourlyWage;
	}

	public void setHourlyWage(float hourlyWage) {
		this.hourlyWage = hourlyWage;
	}

	public float getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(float hoursWorked) {
		this.hoursWorked = hoursWorked;
	}

	public float earning() {
		float earn = 0;
		if (hoursWorked <= 40)
			earn = hourlyWage * hoursWorked;
		else if (hoursWorked > 40) {
			earn = 40 * hourlyWage + (hoursWorked - 40) * 1.5f * hourlyWage;
		}
		return earn;
	}

	public String toString() {
		return String.format("%sHourly wage : $%.2f\nHours Workrd : %.2f\nEarning of Hourly Employee  : $%.2f",
				super.toString(), this.hourlyWage, this.hoursWorked, this.earning());
	}

}

class CommissionEmployee extends Employee {
	private float grossSales;
	private float commissionRate;

	public CommissionEmployee() {
		super();
	}

	public CommissionEmployee(String firstName, String lastName, int socialSecurityNumber, float gs, float cr) {
		super(firstName, lastName, socialSecurityNumber);
		this.commissionRate = cr;
		this.grossSales = gs;
	}

	public float getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(float grossSales) {
		this.grossSales = grossSales;
	}

	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public float earning() {
		float earn = 0;
		earn = commissionRate * grossSales;
		return earn;
	}

	public String toString() {
		return String.format("%sGross Sale : $%.2f\nCommission Rate : %.2f\nEarning of Commission Employee  : $%.2f",
				super.toString(), this.grossSales, this.commissionRate, this.earning());
	}

}

class BaseplusCommissionEmployee extends Employee {
	private float grossSales;
	private float commissionRate;
	private float baseSalary;

	public BaseplusCommissionEmployee() {
		super();
	}

	public BaseplusCommissionEmployee(String firstName, String lastName, int socialSecurityNumber, float gs, float cr,
			float bs) {
		super(firstName, lastName, socialSecurityNumber);
		this.baseSalary = bs;
		this.commissionRate = cr;
		this.grossSales = gs;
	}

	public float getGrossSales() {
		return grossSales;
	}

	public void setGrossSales(float grossSales) {
		this.grossSales = grossSales;
	}

	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public float getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(float baseSalary) {
		this.baseSalary = baseSalary;
	}

	public float earning() {
		float earn = 0;
		earn = (commissionRate * grossSales) + baseSalary;
		return earn;
	}

	public String toString() {
		return String.format(
				"%sGross Sale : $%.2f\nCommission Rate : %.2f\nBase Salary : $%.2f\nEarning of Commission Employee  : $%.2f",
				super.toString(), this.grossSales, this.commissionRate, this.baseSalary, this.earning());
	}

}

public class Program {

	public static void main(String[] args) {

		// SalariedEmployee salariedEmployee = new SalariedEmployee("Lokesh", "Mahajan", 222, 1000);
		// System.out.println(salariedEmployee);

		// HourlyEmployee hourlyEmployee = new HourlyEmployee("Om", "Mahajan", 333, 30, 45);
		// System.out.println(hourlyEmployee);

		// CommissionEmployee commissionEmployee = new CommissionEmployee("Niraj", "Mahajan", 444, 25000, 0.2f);
		// System.out.println(commissionEmployee);

		// BaseplusCommissionEmployee baseplusCommissionEmployee = new BaseplusCommissionEmployee("Nupur", "Mahajan",555, 30000, 0.1f, 1200);
		// System.out.println(baseplusCommissionEmployee);

		Employee[] arr = new Employee[4];
		arr[0] = new SalariedEmployee("Lokesh", "Mahajan", 222, 1000);
		arr[1] = new HourlyEmployee("Om", "Mahajan", 333, 30, 45);
		arr[2] = new CommissionEmployee("Niraj", "Mahajan", 444, 25000, 0.2f);
		arr[3] = new BaseplusCommissionEmployee("Nupur", "Mahajan", 555, 30000, 0.1f, 1200);
		for (int index = 0; index < arr.length; index++) {
			if (arr[index] instanceof BaseplusCommissionEmployee)
				((BaseplusCommissionEmployee) arr[index])
						.setBaseSalary(((BaseplusCommissionEmployee) arr[index]).getBaseSalary() * 1.10f);
			System.out.println(arr[index]);
			System.out.println("-----------------------------------------------------------------");
		}

	}

}
