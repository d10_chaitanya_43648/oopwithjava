package test;

import java.util.Scanner;

abstract class Calculate{
	protected float area;
	protected float perimeter;
	public final float getArea() {
		return area;
	}
	public abstract void setArea();
	public final float getPerimeter() {
		return perimeter;
	}
	public abstract void setPerimeter();
	
}
class Circle extends Calculate {
	private float radius;
	
	public Circle() {
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public void setArea() {
		this.area = (float) ((double)Math.PI * this.radius * this.radius);		
	}
	
	public void setPerimeter() {
		this.perimeter = (float) (2 * Math.PI * this.radius);
	}

}

class Rectangle extends Calculate{
	private float length;
	private float width;
	
	public Rectangle() {
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}
	
	public void setArea() {
		this.area = this.length * this.width;		
	}
	
	public void setPerimeter() {
		this.perimeter = 2 * (this.length + this.width);
	}

}

class Triangle extends Calculate{
	private float base;
	private float height;
	private float side1;
	private float side2;
	
	public Triangle() {
	}

	public float getSide1() {
		return side1;
	}

	public void setSide1(float side1) {
		this.side1 = side1;
	}

	public float getSide2() {
		return side2;
	}

	public void setSide2(float side2) {
		this.side2 = side2;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}
	
	public void setArea() {
		this.area = this.base * this.height / 2;		
	}
	
	public void setPerimeter() {
		this.perimeter = this.side1 + this.side2 + this.base;
	}

}

class Test {
	private Calculate calculate;
	private static Scanner sc = new Scanner(System.in);
	public void setCalculate(Calculate calculate) {
		this.calculate = calculate;
	}

	public void acceptRecord() {
		if(calculate != null) {
			if(calculate instanceof Rectangle) {
				Rectangle rectangle = (Rectangle) calculate;
				System.out.print("Enter length : ");
				rectangle.setLength(sc.nextFloat());
				System.out.print("Enter Width : ");
				rectangle.setWidth(sc.nextFloat());
			}else if(calculate instanceof Circle) {
				Circle circle = (Circle) calculate;
				System.out.print("Enter Radius : ");
				circle.setRadius(sc.nextFloat());
			}else {
				Triangle tri = (Triangle) calculate;
				System.out.print("Enter Base : ");
				tri.setBase(sc.nextFloat());
				System.out.print("Enter Height : ");
				tri.setHeight(sc.nextFloat());
				System.out.print("Enter Side-1 : ");
				tri.setSide1(sc.nextFloat());
				System.out.print("Enter Side-2 : ");
				tri.setSide2(sc.nextFloat());
			}
			this.calculate.setArea( );
			this.calculate.setPerimeter( );			
		}
	}
	
	public void printRecord() {
		if(calculate != null) {
			System.out.println("Area : "+this.calculate.getArea());
			System.out.println("Perimeter : "+this.calculate.getPerimeter());
		}
	}

	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Rectangle");
		System.out.println("2. Circle");
		System.out.println("3. Triangle");
		System.out.print("Enter Choice :");
		return sc.nextInt();
	}
}

class CalculateFactory{
	public static Calculate getInstance(int choice ) {
		Calculate calculate = null;
		switch( choice ) {
		case 1:
			calculate = new Rectangle();	//Upcasting
			break;
		case 2:
			calculate = new Circle();	//Upcasting
			break;
		case 3:
			calculate = new Triangle();	//Upcasting
			break;
		}
		return calculate;
	}
}

public class Program {
	
    public static void main(String[] args) {
    	int choice;
		Test test = new Test();
		while( ( choice = Test.menu( ) ) != 0 ) {
			Calculate calculate = CalculateFactory.getInstance(choice);
			if( calculate != null ) {
				test.setCalculate(calculate);
				test.acceptRecord();
				test.printRecord();
			}
		}
    }
}