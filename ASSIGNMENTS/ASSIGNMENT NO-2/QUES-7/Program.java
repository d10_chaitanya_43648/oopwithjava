/*  7. Create an application that calculates your daily driving cost, so that you can estimate how much money could be saved by car pooling, which also has other advantages such as reducing carbon emissions and reducing traffic congestion. The application should input the following information and display the user’s cost per day of driving to work:
a) Total miles driven per day.
b) Cost per gallon of gasoline.
c) Average miles per gallon.
d) Parking fees per day.
e) Tolls per day.   */

import java.util.Scanner;
class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Total miles driven per day : ");
        float total_miles = sc.nextFloat();
        System.out.print("Enter Cost per gallon of gasoline : ");
        float cost_per_gallon = sc.nextFloat();
        System.out.print("Enter Average miles per gallon : ");
        float avg = sc.nextFloat();
        System.out.print("Enter Parking fees per day : ");
        float parking_fee = sc.nextFloat();
        System.out.print("Enter Tolls per day : ");
        float toll = sc.nextFloat();

        float total_cost;
        total_cost = parking_fee + toll + ( cost_per_gallon * total_miles ) / avg;

        System.out.print("Your daily driving cost : "+total_cost);  
    }
}


/*
1 gallon = 3.78541 litre

a) Total miles driven per day.
b) Cost per gallon of gasoline. =>100
c) Average miles per gallon. =>70
d) Parking fees per day.
e) Tolls per day.

70                      100
Total miles per day     ?

? = (100 * Total miles per day) / 70


Total cost = Parking fees + Tolls + ?

*/