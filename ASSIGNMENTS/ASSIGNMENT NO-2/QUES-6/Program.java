/*  6. Create a BMI (Body Mass Index) calculator that reads the user’s weight in pounds and height in inches (or, if you prefer,
the user’s weight in kilograms and height in meters), then calculates and displays the user’s body mass index.
The formula for calculating BMI is
 BMI = (WeightInKiloGrams) / (HeightInMeters * HeightInMeters);
BMI VALUES
Underweight if BMI is less than 18.5 
Normal if BMI is in between 18.5 and 24.9 
Overweight if BMI is in between 25 and 29.9 
Obese if BMI is 30 or greater    */

import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Weight(in kgs) : ");
        float w = sc.nextFloat();
        System.out.print("Enter Height(in m) : ");
        float h = sc.nextFloat();
        float BMI;
        BMI = w / ( h * h );
        System.out.printf("Your Body Mass Index : %.1f\n", BMI);
        if(BMI < 18.5 )
            System.out.println("Your weight is Underweight");
        else{
            if((BMI >= 18.5) && (BMI <= 24.9)) 
                System.out.println("Your weight is Normal");
            else{ 
                if((BMI >= 25) && (BMI <= 29.9))
                    System.out.println("Your weight is Overweight");
                else
                    System.out.println("Your weight is Obese");
            }
        }
    }
}