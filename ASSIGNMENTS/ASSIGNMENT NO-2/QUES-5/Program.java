/*  5. Modify above assignment and write a menu driven program. Accept input from command prompt.   */

import java.util.Scanner;
class Program{
    public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int choice, number1, number2, result;
    
    do{
        System.out.println("0. Exit");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");

        System.out.println("Enter Choice : ");
        choice = sc.nextInt();

        switch(choice)
        {
            case 1://Addition
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 + number2;
                System.out.println("Addition of two number : "+result);
            break;
            case 2://Subtraction
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 - number2;
                System.out.println("subtraction of two number : "+result);
            break;
            case 3://Multiplication
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                result = number1 * number2;
                System.out.println("multiplication of two number : "+result);
            break;
            case 4:// Division
                System.out.println("Enter Number1 : ");
                number1 = sc.nextInt();
                System.out.println("Enter Number2 : ");
                number2 = sc.nextInt();
                if(number2 == 0)
                    System.out.println("Divide By Zero ");
                else{
                    result = number1 / number2;
                    System.out.println("division of two number : "+result);
                }
            break;
        }
    }while(choice != 0);
    }
}