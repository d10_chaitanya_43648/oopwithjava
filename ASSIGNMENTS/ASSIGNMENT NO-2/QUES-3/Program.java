/*  3. Accept one integer number from command line and provide the following functionalities
- Sum of digits. [e.g. for 1234 , 1 + 2 + 3 + 4 = 10]
- Reverse number [e.g. for 1234 ,4321]
    Check weather given number is palindrome or not. [Check       reverse and original no. are same or not]
- Perfect number. 28 = 1 + 2 + 4 + 7 + 14
    [Perfect number is a positive integer that is the sum of its proper positive divisors]
- Strong number. 145=> 1! + 4! + 5! = 1 + 24 + 120 = 145
    [The sum of the factorials of digits of a number is equal to the original number.]
- Armstrong number. 153 => 13 + 53 +33 = 1+125+27= 153
    [The sum of the cubes of digits of a number is equal to the original number.]
- Prime number. 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97.
    [A prime number is a natural number that has exactly two distinct natural number divisors: 1 and itself.]   */

import java.util.Scanner;
class Program{

    //For Prime Number
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int n, Num = 2;
        for(Num = 2; Num < number; Num++)
        {
            if(number%Num == 0){
                System.out.println(number+" is not Prime Number");
                break;
            }

        }
        if(number == Num)
            System.out.println(number+" is Prime Number");
    }

    //For Armstrong Number
    public static void main5(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int remainder, sum = 0, Original_Num;
        Original_Num = number;
        while(number != 0)
        {
            remainder = number%10;
            sum = sum + (int)Math.pow((double)remainder, 3);
            number = number / 10;
        }
        if(Original_Num == sum)
            System.out.println(Original_Num+" is Armstrong Number");
        else
            System.out.println(Original_Num+" is not Armstrong Number");
    }

    //For Strong Number
    public static void main4(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int remainder, n, sum = 0, Original_Num, i;
        Original_Num = number;
        while(number != 0)
        {
            remainder = number%10;
            for(i = 1, n = 1; i <= remainder; i++)
            {
                n = n * i;
            }
            number = number / 10;
            sum = sum + n;
        }
        if(Original_Num == sum)
            System.out.println(Original_Num+" is Strong Number");
        else
            System.out.println(Original_Num+" is not Strong Number");
    }

    //For Perfect Number
    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int num, n = 0;
        for(num = 1; num < number; num++)
        {
            if(number%num == 0)
            {
                n = n + num;
            }
        }
        if(number == n)
            System.out.println(number+" is Perfect Number");
        else
            System.out.println(number+" is not Perfect Number");
    }

    //For Reverse Number and Palindrome Number
    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int temp, remainder = 0, Original_Num;
        Original_Num = number;
        while(number != 0 )
        {
            temp = number%10;
            remainder = remainder * 10 + temp;
            number = number/10;
        }
        System.out.println("Reverse Number of "+Original_Num+" : "+remainder);

        if(Original_Num == remainder)
            System.out.println("Number is also Palindrome");
        else
            System.out.println("Number is Not Palindrome");
    }

    // Sum of Digits of given number
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number : ");
        int number = sc.nextInt();
        int temp, sum = 0;
        while(number != 0 )
        {
            temp = number%10;
            number = number/10;
            sum = sum + temp;
        }
        System.out.println("Sum : "+sum);
    }
}