class program{
    public static void main (String[] args){

        int num1 = (int) 9348.98;
        System.out.println("Double to Integer : "+num1);

        long num2 =  100;
        System.out.println("Integer to LONG : "+num2);

        short num3 = (short)80999;
        System.out.println("Integer to short : "+num3);

        long num4 = (long)2373467e18;
        System.out.println("Integer to Long : "+num4);

        byte num5 = (byte)129;
        System.out.println("Integer to Byte : "+num5);

        float num6 = (float)218.928;
        System.out.println("Double to Float : "+num6);

        double num7 = (double)2930.3f;
        System.out.println("Float to Double : "+num7);

        char num8 = (char)-3;
        System.out.println("Unsigned Integer to Char : "+num8);

        //boolean num9 = (boolean)0;    //Error int cannot convert into boolean
        //System.out.println("Integer to Long Integer : "+num9);
        
       System.out.println("Size of Integer(in Bits) : "+Integer.SIZE);
       System.out.println("Size of Long(in Bits) : "+Long.SIZE);
       System.out.println("Size of Short(in Bits) : "+Short.SIZE);
       System.out.println("Size of Double(in Bits) : "+Double.SIZE);
       System.out.println("Size of Byte(in Bits) : "+Byte.SIZE);
       //System.out.println("Size of Boolean : "+Boolean.SIZE); //error
       System.out.println("Size of Float(in Bits) : "+Float.SIZE);
       System.out.println("Size of Char(in Bits) : "+Character.SIZE);
       
    }
}