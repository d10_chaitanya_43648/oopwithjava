/*  9. Write a program to accept 3 digits and print all possible combination of these three digits
(For example if three digits are 1, 2 and 3 then all possible combinations are 123,132,231,213,321 and 312) */

import java.util.Scanner;
class Program{
    public static void main(String[] args){
        
        int[] input = null;
        input = new int[3];
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first digit : ");
        input[0] = sc.nextInt();
        System.out.println("Enter second digit : ");
        input[1] = sc.nextInt();
        System.out.println("Enter third digit : ");
        input[2] = sc.nextInt();
        
        for (int x = 0; x < 3; x++) 
        {
            for (int y = 0; y < 3; y++) 
            {
                for (int z = 0; z < 3; z++) 
                {
                    if (x != y && y != z && z != x){
                        System.out.println(input[x] + "" + input[y] + "" + input[z]);
                    }
                }
            }
        }
    }
}