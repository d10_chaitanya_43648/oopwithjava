
/*  4. Write a program for addition, subtraction, multiplication and division of two numbers.   */

import java.util.Scanner;
class Program{
    //division
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        if(number2 == 0)
            System.out.println("Divide By Zero ");
        else{
        result = number1 / number2;
        System.out.println("division of two number : "+result);
        }
    }

    //multiplication
    public static void main3(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 * number2;
        System.out.println("multiplication of two number : "+result);
    }

    //subtraction
    public static void main2(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 - number2;
        System.out.println("subtraction of two number : "+result);
    }

    //Addition
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number1 : ");
        int number1 = sc.nextInt();
        System.out.println("Enter Number2 : ");
        int number2 = sc.nextInt();
        int result;
        result = number1 + number2;
        System.out.println("Addition of two number : "+result);
    }
}