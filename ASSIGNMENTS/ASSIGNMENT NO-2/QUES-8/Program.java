/*  8. Input the current world population and the annual world population growth rate. Write an application to display the estimated world population after one, two, three, four and five years.   */

import java.util.Scanner;

class Program{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter current world population : ");
        long cur_pop = sc.nextLong();
        System.out.print("Enter annual world population growth rate : ");
        float rate = sc.nextFloat();

        int n = 1;
        long population;

        for(n = 1; n <= 5; n++)
        {
            population = Math.round((double)cur_pop * Math.pow( (1 + (rate/100)), n));
            System.out.println("Estimated world population after "+n+" years : "+population);
        }
    }
}

/* 
P = population at now
R = rate per annum

Population after n years = P ((1 + (R/100))^n)

Population n years ago = P /((1 + (R/100))^n)
*/