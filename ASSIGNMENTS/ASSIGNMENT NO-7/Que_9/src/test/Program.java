package test;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] str = new String[1];
		System.out.print("Enter String : ");
		str[0] = sc.nextLine();
		System.out.print("Enter Char  : ");
		char ch = sc.next().charAt(0);
		Program.indexOf(str[0], ch);
		Program.lastIndexOf(str[0], ch);

	}

	private static void lastIndexOf(String string, char ch) {
		for (int index = string.length() - 1; index >= 0; index--) {
			if (ch == string.charAt(index)) {
				System.out.println("Index of " + ch + " is : " + index);
				break;
			}
		}
	}

	private static void indexOf(String string, char ch) {
		for (int index = 0; index < string.length(); index++) {
			if (ch == string.charAt(index)) {
				System.out.println("Index of " + ch + " is : " + index);
				break;
			}
		}
	}

}
