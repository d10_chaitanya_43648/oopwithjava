package test;

import java.util.StringTokenizer;

public class Program {

	public static void main(String[] args) {
		String name = args[ 0 ];
		String delim = "."; 
		StringTokenizer stk = new StringTokenizer(name, delim); 
		while( stk.hasMoreTokens()) { 
			String token = stk.nextToken(); 
			System.out.println(token); 
		}
	}

}
