package test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		// System.out.print("Enter Date in MM/DD/YYYY : ");
		// String str = sc.nextLine();
		String str = "04/25/1955";
		String[] s = str.split("/");

		Date date = new Date(Integer.parseInt(s[2]) - 1900, Integer.parseInt(s[0]) - 1, Integer.parseInt(s[1]));
		String pattern = "MMMM dd, yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String strDate = sdf.format(date);
		System.out.println(strDate);

	}

}
