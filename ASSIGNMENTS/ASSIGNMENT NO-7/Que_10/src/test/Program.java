package test;

import java.util.Random;
import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter String of size 5 : ");
		String str = sc.nextLine();
		String[] c = new String[5];
		for (int index = 0; index < str.length(); index++) {
			c[index] = str.substring(index, index + 1);
		}
		Random r = new Random();
		int cnt = 30;
		while (cnt != 0) {
			System.out.println(c[r.nextInt(5)] + c[r.nextInt(5)] + c[r.nextInt(5)]);
			cnt--;
		}
	}

}
