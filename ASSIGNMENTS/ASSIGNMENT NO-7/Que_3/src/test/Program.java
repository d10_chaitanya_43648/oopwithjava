package test;

import java.util.Random;

public class Program {

	public static void main(String[] args) {

		for (int index = 0; index < 20; index++)
			System.out.println(Program.sentences());
	}

	private static String sentences() {
		String[] article = { "the", "a", "one", "some", "any" };
		String[] noun = { "boy", "girl", "dog", "town", "car" };
		String[] verb = { "drove", "jumped", "ran", "walked", "skipped" };
		String[] preposition = { "to", "from", "over", "under", "on" };

		Random r = new Random();
				String str = article[r.nextInt(article.length)];
				String fstr = str.substring(0, 1).toUpperCase();
				String rstr = str.substring(1);
		return fstr+rstr+" "+noun[r.nextInt(noun.length)]+" "+verb[r.nextInt(verb.length)]+" "+preposition[r.nextInt(preposition.length)]+" "+article[r.nextInt(article.length)]+" "+noun[r.nextInt(noun.length)]+":";
	
	}

}
