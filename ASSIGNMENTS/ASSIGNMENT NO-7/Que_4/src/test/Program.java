package test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the telephone number : ");
		String str = sc.nextLine();
		String delim = "()- "; 
		StringTokenizer stk = new StringTokenizer(str, delim);
		String[] s = new String[3];
		int index = 0; 
		while( stk.hasMoreTokens()) { 
				s[index] = stk.nextToken();
				index++;
			}
		System.out.println("Area Code : "+s[0]+"     Phone number : "+(s[1]+s[2]));
	}

}
