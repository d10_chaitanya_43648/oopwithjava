package test;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc  = new Scanner(System.in);
		System.out.print("Enter First String : ");
		String str1 = sc.nextLine();
		System.out.print("Enter Second String : ");
		String str2 = sc.nextLine();

		if(str1.regionMatches(true, 3, str2, 0, 3)) {
			System.out.println("Both String are Equal...");			
		}else
			System.out.println("Both String are Not Equal...");
		
	}

}
