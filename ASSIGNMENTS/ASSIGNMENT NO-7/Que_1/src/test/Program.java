package test;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc  = new Scanner(System.in);
		System.out.print("Enter First String : ");
		String str1 = sc.nextLine();
		System.out.print("Enter Second String : ");
		String str2 = sc.nextLine();

		if((str1.compareTo(str2)) == 0) {
			System.out.println("Both String are Equal...");			
		}else if((str1.compareTo(str2)) > 0 )
			System.out.println("String First is Greater than Second...");
		else 
			System.out.println("String First is Less than Second...");

		
	}

}
