/* 2. Write a java program to check type compatibility for following statements. Observe the effect. Make changes in terms of casting if needed and also display the width of all the above data types.
* int : 9348.39
* long int : 100
* short : 80999
* long : 2373467e18
* byte : 129
* float : 218.928
* double : 2930.3f
* char : -3
* boolean : 0   */

class Program{
    public static void main(String[] args) {
        int num1 = (int)9348.39;
        System.out.println("Double to Integer : "+num1);
        long num2 = (long)100;
        System.out.println("Integer to Long : "+num2);
        short num3 = (short)80999;
        System.out.println("Integer to short : "+num3);
        long num4 = (long)2373467e18;
        System.out.println("Integer to Long : "+num4);
        byte num5 = (byte)129;
        System.out.println("Integer to Byte : "+num5);
        float num6 = (float)218.928;
        System.out.println("Double to Float : "+num6);
        double num7 = (double)2930.3f;
        System.out.println("Float to Double : "+num7);
        char num8 = (char)-3;
        System.out.println("Unsigned Integer to Char : "+num8);
        //boolean num9 = (boolean)0;    //Error int cannot convert into boolean
        //System.out.println("Integer to Long Integer : "+num9);
       System.out.println("Size of Integer(in Bits) : "+Integer.SIZE);
       System.out.println("Size of Long(in Bits) : "+Long.SIZE);
       System.out.println("Size of Short(in Bits) : "+Short.SIZE);
       System.out.println("Size of Double(in Bits) : "+Double.SIZE);
       System.out.println("Size of Byte(in Bits) : "+Byte.SIZE);
       //System.out.println("Size of Boolean : "+Boolean.SIZE); //error
       System.out.println("Size of Float(in Bits) : "+Float.SIZE);
       System.out.println("Size of Char(in Bits) : "+Character.SIZE);
       
    }
}