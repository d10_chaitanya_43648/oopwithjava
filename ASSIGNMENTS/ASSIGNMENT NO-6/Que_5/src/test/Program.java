package test;

import java.util.Arrays;
import java.util.Comparator;

abstract class Shape{
	protected float area;
	protected float perimeter;
	public Shape() {
		
	}
	public Shape(float area, float perimeter) {
		this.area = area;
		this.perimeter = perimeter;
	}
	public final float getArea() {
		return area;
	}
	public abstract void setArea();
	public final float getPerimeter() {
		return perimeter;
	}
	public abstract void setPerimeter();
	public abstract String toString();
	
}

class Rectangle extends Shape{
	private float length;
	private float width;
	public Rectangle(float length, float width) {
		this.length = length;
		this.width = width;
		this.setArea();
		this.setPerimeter();
	}
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	
	public void setArea() {
		this.area = this.length * this.width;
	}
	
	public void setPerimeter() {
		this.perimeter = 2 * (this.length + this.width );
	}
	public String toString() {
		return String.format("Area : %.2f     Perimeter : %.2f", this.area, this.perimeter);
	}
	
}

class Circle extends Shape{
	private float radius;

	public Circle(float radius) {
		this.radius = radius;
		this.setArea();
		this.setPerimeter();
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public void setArea() {
		this.area = (float) (Math.PI * this.radius * this.radius);
	}
	
	public void setPerimeter() {
		this.perimeter = (float) (2 * Math.PI * this.radius);
	}
	public String toString() {
		return String.format("Area : %.2f     Perimeter : %.2f", this.area, this.perimeter);
	}
}

class Triangle extends Shape{
	private float base;
	private float height;
	private float side1;
	private float side2;
	public Triangle(float base, float height, float side1, float side2) {
		this.base = base;
		this.height = height;
		this.side1 = side1;
		this.side2 = side2;
		this.setArea();
		this.setPerimeter();
	}
	public float getBase() {
		return base;
	}
	public void setBase(float base) {
		this.base = base;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getSide1() {
		return side1;
	}
	public void setSide1(float side1) {
		this.side1 = side1;
	}
	public float getSide2() {
		return side2;
	}
	public void setSide2(float side2) {
		this.side2 = side2;
	}
	
	public void setArea() {
		this.area = this.base * this.height / 2;
	}
	
	public void setPerimeter() {
		this.perimeter = this.base + this.side1 + this.side2;
	}
	public String toString() {
		return String.format("Area : %.2f     Perimeter : %.2f", this.area, this.perimeter);
	}
	
}

class SortByArea implements Comparator<Shape>{
	public int compare(Shape s1, Shape s2) {		
		return (int) (s1.getArea() - s2.getArea());
	}	
}

class SortByPerimeter implements Comparator<Shape>{
	public int compare(Shape s1, Shape s2) {		
		return (int) (s1.getPerimeter() - s2.getPerimeter());
	}	
}

public class Program {
	
	public static Shape[] getInstance() {
		Shape[] arr = new Shape[3];
		arr[ 0 ] = new Rectangle(10f, 20f);
		arr[ 1 ] = new Circle(10f);
		arr[ 2 ] = new Triangle(10f, 15f, 20f, 25f);
		return arr;
	}
	
	public static void print(Shape[ ] arr) {
		if(arr != null) {
			for(Shape shape : arr) {
				if(shape instanceof Rectangle ) {
					Rectangle rect = (Rectangle) shape;
					System.out.println(rect.toString());
				}else if(shape instanceof Circle ) {
					Circle c = (Circle) shape;
					System.out.println(c.toString());
				}else if(shape instanceof Triangle ) {
					Triangle t = (Triangle) shape;
					System.out.println(t.toString());
				}
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		Shape [] arr = Program.getInstance();
		Program.print(arr);
		Comparator<Shape> c = null;
		
		c = new SortByArea();
		Arrays.sort(arr, c);
		Program.print(arr);
		
		c = new SortByPerimeter();
		Arrays.sort(arr, c);
		Program.print(arr);
		
	}

}