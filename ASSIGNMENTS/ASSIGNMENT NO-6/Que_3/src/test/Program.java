package test;

import java.util.Arrays;
import java.util.Comparator;

class Address {
	private String cityName;
	private String stateName;
	private String pinCode;

	public Address(String cityName, String stateName, String pinCode) {
		this.cityName = cityName;
		this.stateName = stateName;
		this.pinCode = pinCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (cityName == null) {
			if (other.cityName != null)
				return false;
		} else if (!cityName.equals(other.cityName))
			return false;
		if (pinCode == null) {
			if (other.pinCode != null)
				return false;
		} else if (!pinCode.equals(other.pinCode))
			return false;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		return true;
	}

	public String toString() {
		return String.format("City Name : %s\nState Name : %s\nPin Code : %s", this.cityName, this.stateName,
				this.pinCode);
	}
}

class SortByState implements Comparator<Address>{
	public int compare(Address add1, Address add2) {
		return add1.getStateName().compareTo(add2.getStateName());
	}
}

public class Program {
	public static Address[] getaddress() {
		Address[] arr = new Address[5];
		arr[0] = new Address("Jalgaon", "MAH", "425301");
		arr[1] = new Address("Bhopal", "MP", "450445");
		arr[2] = new Address("Agra", "Delhi", "725001");
		arr[3] = new Address("Chennai", "TAMILNADU", "300142");
		arr[4] = new Address("Bhuwaneshwar", "ORISSA", "600325");
		return arr;
	}

	public static void print(Address [] arr) {
		if(arr != null) {
			for(Address add : arr) {
				System.out.println(add.toString());
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Address [] address = Program.getaddress();
		Program.print(address);
		Comparator<Address> c = null;
		c = new SortByState();
		Arrays.sort(address, c);
		Program.print(address);

	}
}