package test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;

class BirthDate {
	private LocalDate ldt;

	public BirthDate(int day, int month, int year) {
		this.ldt = LocalDate.of(year, month, day);
	}

	public LocalDate getLdt() {
		return ldt;
	}

	public void setLdt(LocalDate ldt) {
		this.ldt = ldt;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BirthDate other = (BirthDate) obj;
		if (ldt == null) {
			if (other.ldt != null)
				return false;
		} else if (!ldt.equals(other.ldt))
			return false;
		return true;
	}

	public String toString() {
		return String.format("Date : %d-%d-%d", this.ldt.getDayOfMonth(), this.ldt.getMonthValue(), this.ldt.getYear());
	}
}

class Person {
	private String name;
	private String city;
	private String mobileNumber;
	private BirthDate bd;

	public Person(String name, String city, String mobileNumber, int day, int month, int year) {
		this.name = name;
		this.city = city;
		this.mobileNumber = mobileNumber;
		this.bd = new BirthDate(day, month, year);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public BirthDate getBd() {
		return bd;
	}

	public void setBd(BirthDate bd) {
		this.bd = bd;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (bd == null) {
			if (other.bd != null)
				return false;
		} else if (!bd.equals(other.bd))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (mobileNumber == null) {
			if (other.mobileNumber != null)
				return false;
		} else if (!mobileNumber.equals(other.mobileNumber))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public String toString() {
		return String.format("Name : %s\nBirthDate : %s\nCity : %s\nMobile no. : %s", this.name, this.bd.toString(), this.city,
				this.mobileNumber);
	}

}

class SortByBirthDate implements Comparator<Person>{
	public int compare(Person bd1, Person bd2) {
		return bd1.getBd().getLdt().compareTo(bd2.getBd().getLdt());
	}
}

public class Program {
	public static Person[] getPerson() {
		Person[] arr = new Person[5];
		arr[0] = new Person("Nikhil", "Jalgaon", "7769061257", 29, 3, 1997);
		arr[1] = new Person("Lokesh", "Mumbai", "1237760257", 6, 4, 2000);
		arr[2] = new Person("Om", "Pune", "1354854551", 15, 8, 1998);
		arr[3] = new Person("Niraj", "Satara", "8830377742", 11, 5, 1997);
		arr[4] = new Person("Mukesh", "Kolhapur", "8806538575", 5, 2, 2002);
		return arr;
	}
	
	public static void print(Person [] arr) {
		if(arr != null) {
			for(Person person : arr) {
				System.out.println(person.toString());
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {

		Person[] arr = Program.getPerson();
		Program.print(arr);
		Comparator<Person> c = null;
		c = new SortByBirthDate();
		Arrays.sort(arr, c);
		Program.print(arr);

	}
}