package test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;

class Date {
	private LocalDate ldt;

	public Date() {
		this.ldt = LocalDate.now();
	}

	public Date(int day, int month, int year) {
		this.ldt = LocalDate.of(year, month, day);
	}

	public LocalDate getLdt() {
		return ldt;
	}

	public void setLdt(LocalDate ldt) {
		this.ldt = ldt;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (ldt == null) {
			if (other.ldt != null)
				return false;
		} else if (!ldt.equals(other.ldt))
			return false;
		return true;
	}	
	
	public String toString() {
		return String.format("Date : %d - %d - %d", this.ldt.getDayOfMonth(), this.ldt.getMonthValue(), this.ldt.getYear());
	}
}

class sortByYear implements Comparator<Date>{
	public int compare(Date dt1, Date dt2) {
		return dt1.getLdt().getYear() - dt2.getLdt().getYear();
	}
	
}
public class Program {
	public static Date[] getdate() {
		Date[] arr = new Date[10];
		arr[0] = new Date(12, 03, 1997);
		arr[1] = new Date(23, 04, 1985);
		arr[2] = new Date(25, 03, 2010);
		arr[3] = new Date(04, 10, 2001);
		arr[4] = new Date(19, 07, 1991);
		arr[5] = new Date(02, 01, 1993);
		arr[6] = new Date(12, 11, 1975);
		arr[7] = new Date(9, 04, 1963);
		arr[8] = new Date(31, 8, 1955);
		arr[9] = new Date(15, 12, 2020);
		return arr;
	}

	public static void print(Date[] arr) {
		if(arr != null) {
			for(Date date : arr) {
				System.out.println(date.toString());
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {

		Date [] arr = Program.getdate();
		Program.print(arr);
		Comparator<Date> c = null;
		c = new sortByYear();
		Arrays.sort(arr, c);
		Program.print(arr);
	}
}